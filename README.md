# Coinche

## An experimental coinche game using DFinity framework.

This project's goal is to learn dfinity, provide examples, test new functionalities, and promote coinche around the world ;-p.

## Getting started

```bash
cd coinche/
./init.sh
./build.sh
```

Once the sever is running, open a browser and type the following url:

`http://127.0.0.1:8200?address=1`


