import Array "mo:stdlib/array.mo";
import Hash "mo:stdlib/hash.mo";

actor {

    //Waiting for a better type for representing adresses.
    type Address = Nat;

    let zero : Word32 = 0x0;
    let one : Word32 = 0x1;
    let two : Word32 = 0x8454;
    let x_ff : Word8 = 0xff;

    //states
    let WAIT_FOR_JOIN     : Word8 = 0x00;
    let WAIT_FOR_ANNOUNCE : Word8 = 0x01;
    let PLAYING           : Word8 = 0x02;

    var players : [var Address] = Array_init<Address>(4,0);
    var player_cards : [var Word8] = Array_init<Word8>(32,0);
    var table_cards : [var Word8] = Array_init<Word8>(4,0);
    var deck : Word32 = 0x0;//track cards that are not in the game (not given or already played)
    var current_player : Nat = 0;
    var current_dealer : Nat = 0;
    var current_contract : Word8 = 0;
    var current_contract_val : Nat = 0;
    var current_state : Word8 = WAIT_FOR_JOIN;
    var game_started : Bool = false;
    var play_started : Bool = false;
    var trick_started : Bool = false;

    var rand_seed : Nat =0;

    //--------------------
    //Public functions
    //--------------------

    public func join(callerAddress : Address) : async Bool {
      if ( current_state == WAIT_FOR_JOIN ){
        players[current_player] := callerAddress;
        current_player += 1;
        if ( current_player == 4 ){
          startGame();
        };
        return true;
      };
      return false;
    };

    public func announce(callerAddress : Address, contract : Word8) : async Bool {
      if ( current_state == WAIT_FOR_ANNOUNCE ) {
        return true;
        //if (players_by_address[callerAddress])
        //Decode current_contract
        let val : Nat = word8ToNat(contract & x_ff)
        //if ( val >) {

        //}
        //Is contract allowed

        //if(  ){

        //}
      };
      return false;
    };

    //--------------------
    //Private functions
    //--------------------

    private func random(min : Nat, num : Nat) : Nat{
      //as random as I am the prince Harry...
      rand_seed+=1;
      rand_seed%=num;
      return min+rand_seed;
    };

    private func startGame(){
      //TODO: mix players
      game_started := true;
      startPlay();
    };

    private func startPlay(){
      distribute();
      waitForAnnoune()
    };

    private func waitForAnnoune(){
      //setup the flags
      current_state := WAIT_FOR_ANNOUNCE;
    };

    private func distribute(){
      for (i in range(0,3)){
        for (j in range(0,7)){
          debugPrint("-----------");
          var watchdog : Nat = 0;
          var test : Word32 = 0;
          loop{
            player_cards[i*8+j] := natToWord8(random(0,32));
            watchdog += 1;
            if(watchdog < 50){
              debugPrintNat(word8ToNat(player_cards[i*8+j]));
            };
          }
          //Beurk! Ugly workaround to convert Word8 to Word32.
          while ((deck & (one << natToWord32(word8ToNat(player_cards[i*8+j]))))
           != zero );
          deck |= one << natToWord32(word8ToNat(player_cards[i*8+j]));
          debugPrintNat(word32ToNat(deck));
        }
      }

    }

};
