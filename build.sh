
ENGINE_DIR=engine

#Rebuild dfinity canister and launch it
cd $ENGINE_DIR

killall client nodemanager dfx
rm -rf /tmp/ic*
rm -rf /tmp/client_config
rm -rf rm -rf /tmp/checkpoints
rm -rf node_modules/.cache

dfx stop && rm -rf canisters && dfx build && dfx start --background && dfx canister install coinche
cd ..
cp output/canisters/coinche/assets/* output/frontend/http/
cp frontend/html/index.html output/frontend/http/
#cp canisters/coinche/assets/ ../frontend
